<?php
class hourGraph extends widget implements widgetInterface {
    const NAME="hourGraph";

    protected $files=array(
        "css" => "widget.css",
        "js" => array(
                "widget.js",
                "https://code.jquery.com/jquery-1.9.1.js",
                "https://code.highcharts.com/highcharts.js",
                "https://code.highcharts.com/highcharts-more.js"
            ),
        "html" => "widget.html"
    );

    protected $parameters=array(
        "field1",
        "field2",
        "field3",
        "field4",
        "max",
        "title"
    );

    public function getUpdate() {
        $field1=$this->getParam("field1");
        $field2=$this->getParam("field2");
        $field3=$this->getParam("field3");
        $field4=$this->getParam("field4");
        return array("current" => "hourGraph.update(" . $this->getName() .
            ", [ '" . $field1 . "'" .
            ",'" . $field2 . "'" .
            ",'" . $field3 . "'" .
            ",'" . $field4 . "'" .
            "], data)");
    }

}
