<?php

namespace collector\dsmr\repository\aggregate;

use aggregate;

use db\clause;
use db\db;
use db\delete;
use db\insert;
use db\select;

use repository as repositoryInterface;



abstract class aggregateRepository implements repositoryInterface {

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($aggregate) {
        return false;
        //$qry=new insert(static::TABLE);

        //foreach ($measurement->getData() as $label => $value) {
        //    $param=new param(":" . $label, $value);
        //    $qry->addParam($param);
       // }
        //$qry->execute();
    }

    public function getAll() {

    }

    public function get($field, $value) {


    }

    protected function Aggregate(aggregateRepository $from, array $groupby) {
        // Delete old data from the measurement table:
        $delete=new delete($from::TABLE);

        $selectmax=new select(static::TABLE);
        $selectmax->addFunction(array("datetime_max" => "max(datetime)"));
        if (substr($selectmax, -1) == ";") {
            $selectmax=substr($selectmax, 0, -1);
        }
        $delete->where(new clause("datetime <= (" . $selectmax . ")"));

        $delete->execute();


        $select=new select($from::TABLE);

        $select->addFields(array("eid"));

        $key=key($groupby);

        $select->addFunction(array(
            $key => current($groupby),
            "T1"    => "min(T1)",
            "T1R"   => "min(T1R)",
            "T2"    => "min(T2)",
            "T2R"   => "min(T2R)",
        ));

        foreach (array(
            "current",
            "currentR",
            "CurrentAmpL1",
            "CurrentAmpL2",
            "CurrentAmpL3",
            "CurrentL1",
            "CurrentL2",
            "CurrentL3",
            "CurrentRL1",
            "CurrentRL2",
            "CurrentRL3") as $field) {

            $select->addFunction(array(
                "avg". $field => "avg(" . $field . ")",
                "min". $field => "min(" . $field . ")",
                "max". $field => "max(" . $field . ")",
            ));
        }

        $select->addFields(array("EidM1"));
        $select->addFunction(array(
            "GasM1DateTime"    => "min(GasM1DateTime)",
            "GasM1"   => "min(GasM1)"
        ));

        $select->addGroupBy($key);

        $insert=new insert(static::TABLE);
        $insert->subquery=$select;

        db::query($insert);

        // Now delete the last line from the table, because that data may be based on an incomplete period
        $delete=new delete(static::TABLE);
        $delete->addOrder("datetime DESC");
        $delete->addLimit(1);
        $delete->allowDeleteAll()->execute();

    }

    protected function createAggregate(repositoryInterface $from, array $groupby) {
        // Delete old data from the measurement table:
        $delete=new delete($from::TABLE);

        $selectmax=new select(static::TABLE);
        $selectmax->addFunction(array("datetime_max" => "max(datetime)"));
        if (substr($selectmax, -1) == ";") {
            $selectmax=substr($selectmax, 0, -1);
        }
        $delete->where(new clause("datetime <= (" . $selectmax . ")"));


        $delete->execute();


        $select=new select($from::TABLE);

        $select->addFields(array("eid"));

        $key=key($groupby);

        $select->addFunction(array(
            $key => current($groupby),
            "T1"    => "min(T1)",
            "T1R"   => "min(T1R)",
            "T2"    => "min(T2)",
            "T2R"   => "min(T2R)",
        ));

        foreach (array(
            "current",
            "currentR",
            "CurrentAmpL1",
            "CurrentAmpL2",
            "CurrentAmpL3",
            "CurrentL1",
            "CurrentL2",
            "CurrentL3",
            "CurrentRL1",
            "CurrentRL2",
            "CurrentRL3") as $field) {

            if ($from instanceof \collector\dsmr\repository\repository) {
                $select->addFunction(array(
                    "avg". $field => "avg(" . $field . ")",
                    "min". $field => "min(" . $field . ")",
                    "max". $field => "max(" . $field . ")",
                ));
            } else {
                $select->addFunction(array(
                    "avg". $field => "avg(avg" . $field . ")",
                    "min". $field => "min(min" . $field . ")",
                    "max". $field => "max(max" . $field . ")",
                ));
            }
        }

        $select->addFields(array("EidM1"));
        $select->addFunction(array(
            "GasM1DateTime"    => "min(GasM1DateTime)",
            "GasM1"   => "min(GasM1)"
        ));

        $select->addGroupBy($key);
        $insert=new insert(static::TABLE);
        $insert->subquery=$select;

        db::query($insert);

        // Now delete the last line from the table, because that data may be based on an incomplete minute:
        $delete=new delete(static::TABLE);
        $delete->addOrder("datetime DESC");
        $delete->addLimit(1);
        $delete->allowDeleteAll()->execute();

    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        return $result->fetchObject("aggregate", array(new static));
    }

}

