<?php
namespace collector\dsmr\obis;
use Exception;

class obisString implements obisValueType {

    public function __construct($minsize=0, $maxsize=0) {


    }

    public function getConvertedValue($data) {
        $value="";
        if (substr($data,0,1) != "(" || substr($data, -1) != ")") {
            throw new Exception("Incorrect data format: " . $data);
        }
        $data=substr($data,1,-1);
        $hexes=str_split($data, 2);
        foreach ($hexes as $hex) {
            $value.=chr(hexdec($hex));
        }
        return $value;
    }

    public function getUnit($data) {
        return null;
    }
}



