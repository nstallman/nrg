<?php
namespace collector\dsmr\obis;

use Exception;

class obisBool implements obisValueType {

    public function __construct() {


    }

    public function getConvertedValue($data) {
        if (substr($data,0,1) != "(" || substr($data, -1) != ")") {
            throw new Exception("Incorrect data format");
        }
        $data=substr($data,1,-1);
        return (bool) $data;
    }

    public function getUnit($data) {
        return null;
    }

}



