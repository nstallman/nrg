<?php
namespace collector\growatt\repository\aggregate;

use repository as repositoryInterface;

class day extends aggregateRepository implements repositoryInterface {
    const TABLE="growatt_aggregate_day";

    public function create($repository) {
        parent::createAggregate($repository, [ "day" => "date_format(datetime, '%Y-%m-%d 00:00:00')" ]);
    }
}

