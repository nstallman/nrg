<?php

namespace collector\growatt\repository\aggregate;

use aggregate;

use db\clause;
use db\db;
use db\delete;
use db\insert;
use db\select;

use repository as repositoryInterface;



abstract class aggregateRepository implements repositoryInterface {

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($aggregate) {
        return false;
        //$qry=new insert(static::TABLE);

        //foreach ($measurement->getData() as $label => $value) {
        //    $param=new param(":" . $label, $value);
        //    $qry->addParam($param);
       // }
        //$qry->execute();
    }

    public function getAll() {

    }

    public function get($field, $value) {


    }

    protected function createAggregate(repositoryInterface $from, array $groupby) {
        // Delete old data from the measurement table:
        $delete=new delete($from::TABLE);

        $selectmax=new select(static::TABLE);
        $selectmax->addFunction(array("datetime_max" => "max(datetime)"));
        if (substr($selectmax, -1) == ";") {
            $selectmax=substr($selectmax, 0, -1);
        }
        $delete->where(new clause("datetime <= (" . $selectmax . ")"));

        $delete->execute();


        $select=new select($from::TABLE);

        $select->addFields(array("serial", "Ident"));

        $key=key($groupby);

        $select->addFunction(array(
            $key => current($groupby)
        ));

        foreach (array(
            "Ppv",
            "Vpv1",
            "Ipv1",
            "Ppv1",
            "Vpv2",
            "Ipv2",
            "Ppv2",
            "Pac",
            "Fac",
            "Vac1",
            "Iac1",
            "Pac1",
            "Vac2",
            "Iac2",
            "Pac2",
            "Vac3",
            "Iac3",
            "Pac3",
            "Temp") as $field) {

            if ($from instanceof \collector\growatt\repository\repository) {
                $select->addFunction(array(
                    "avg". $field => "avg(" . $field . ")",
                    "min". $field => "min(" . $field . ")",
                    "max". $field => "max(" . $field . ")",
                ));
            } else {
                $select->addFunction(array(
                    "avg". $field => "avg(avg" . $field . ")",
                    "min". $field => "min(min" . $field . ")",
                    "max". $field => "max(max" . $field . ")",
                ));
            }
        }

        $select->addFunction(array(
            "EacToday"  => "min(EacToday)",
            "EacTotal"  => "min(EacTotal)",
            "Epv1Total" => "min(Epv1Total)",
            "Epv1Today" => "min(Epv1Today)",
            "Epv2Total" => "min(Epv2Total)",
            "Epv2Today" => "min(Epv2Today)",
            "Tall"      => "min(Tall)",
        ));

        $select->addGroupBy($key);

        $insert=new insert(static::TABLE);
        $insert->subquery=$select;

        db::query($insert);

        // Now delete the last line from the table, because that data may be based on an incomplete period
        $delete=new delete(static::TABLE);
        $delete->addOrder("datetime DESC");
        $delete->addLimit(1);
        $delete->allowDeleteAll()->execute();

    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        return $result->fetchObject("aggregate", array(new static));
    }

}

