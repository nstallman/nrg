<?php
namespace collector\growatt\repository;

use repository as repositoryInterface;

use db\db;
use db\clause;
use db\delete;
use db\insert;
use db\select;

class memory extends repository implements repositoryInterface {
    const TABLE="growatt_mem";


    public static function commitToDB(repository $db) {
        // First delete anything from the memory db that is already in the
        // disk db:

        $delete=new delete(static::TABLE);

        $selectmax=new select($db::TABLE);
        $selectmax->addFunction(array("datetime_max" => "max(datetime)"));
        if (substr($selectmax, -1) == ";") {
            $selectmax=substr($selectmax, 0, -1);
        }
        $delete->where(new clause("datetime <= (" . $selectmax . ")"));

        /* @todo: check if this can be run with a normal query execute */
        db::SQL($delete);

        // Now flush the memory db to disk:

        $insert=new insert($db::TABLE);
        $select=new select(self::TABLE);

        $insert->subquery=$select;
        db::query($insert);


    }

}

