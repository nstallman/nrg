<?php

class aggregate {
    private $repo;
    private $data=array();

    public function __construct(repository $repo) {
        $this->repo=$repo;
    }

    public function create(repository $measurements) {
        return $this->repo->create($measurements);

    }

    public function getData() {
        return $this->data;
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getLast() {
        return $this->repo->getLast();
    }

    public function getLastHour() {
        return $this->repo->getLastHour();
    }


}
